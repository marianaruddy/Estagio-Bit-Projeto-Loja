package hitstore

import grails.gorm.transactions.Transactional

@Transactional
class ProductService {

    def save(Product product) {
        product.save()
    }
}
