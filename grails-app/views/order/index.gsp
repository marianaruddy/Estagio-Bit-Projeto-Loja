<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="layout" content="main"/>
    <title>Lista de pedidos</title>
</head>

<body>
<h1>Lista de pedidos:</h1>

<table>
    <tr>
        <th>IdPedido</th>
        <th>Nome</th>
        <th>Endereço</th>
        <th>Quantidade</th>
        <th>Produto ID</th>
        <th>Status</th>
        <th>Valor total</th>
    </tr>
    <g:each in="${orderList}">
        <tr>
            <td>${it.id}</td>
            <td>${it.name}</td>
            <td>${it.adress}</td>
            <td>${it.amount}</td>
            <td>${it.productId}</td>
            <td>${it.status}</td>
            <td>${it.total}</td>
        </tr>
    </g:each>
</table>
</body>
</html>