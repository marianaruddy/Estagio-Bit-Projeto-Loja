<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <meta name="layout" content="main"/>
        <title>Formulário de Produto</title>
    </head>

    <body>
        <h1>${params.action == 'create' ? "Adicionar produto:" : "Editar produto:"}</h1>
        <g:uploadForm url="[action: 'save', controller: 'product']" method="post">
            <g:if test="${product?.id}">
                <input type="hidden" name="id" value="${product?.id}">
            </g:if>
            Nome:               <g:textField name="name" value="${product?.name}" /> <br>
            Descrição:          <g:textField name="description" value="${product?.description}" /> <br>
            Status:             <g:checkBox name="status" value="${true}" /> <br>
            Preço:              <g:field name="fullPrice" type="number" min="0" step="0.01" value="${product?.fullPrice}" /> <br>
            Preço com desconto: <g:field name="discountPrice" type="number" min="0" step="0.01" value="${product?.discountPrice}" /> <br>
            <g:if test="${params.action == 'create'}">
                Imagem:         <g:field name="image" type="file" value="" />
            </g:if>
            Quantidade:         <g:field name="stock" type="number" min="0" value="${product?.stock}" /> <br>
            <g:submitButton name="submitButton" value="Salvar Produto" />
        </g:uploadForm>

        <a href="${g.createLink(action: 'index')}">
            <button type="button">Voltar</button>
        </a>

    </body>
</html>