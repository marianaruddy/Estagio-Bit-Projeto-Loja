<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <meta name="layout" content="main"/>
        <title>Usuários</title>
    </head>

    <body>
        <g:uploadForm url="[action: 'save', controller: 'user']" method="post">
            Nome:           <g:textField name="username" /> <br>
            Senha:          <g:textField name="password" /> <br>
            Email:          <g:field type="email" name="email" /> <br>
            Endereço:       <g:textField name="adress" /> <br>
            <g:submitButton name="submitButton" value="Cadastrar" />
        </g:uploadForm>
    </body>
</html>