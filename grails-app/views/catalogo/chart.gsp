<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <meta name="layout" content="main"/>
        <title>Checkout</title>
    </head>

    <body>
        <h1>Compra:</h1>

        <table>
            <tr>
                <th>Imagem</th>
                <th>Nome</th>
                <th>Descrição</th>
                <th>Preço Total</th>
                <th>Preço Desconto</th>
                <th>Estoque</th>
            </tr>
            <tr>
                <td>
                    <img src="data:image/png;base64,${product.image.encodeBase64()}"/>
                </td>
                <td>${product.name}</td>
                <td>${product.description}</td>
                <td>${product.fullPrice}</td>
                <td>${product.discountPrice}</td>
                <td>${product.stock}</td>
            </tr>
        </table>

        <g:uploadForm url="[action: 'checkout', controller: 'catalogo']" method="post">
            <input type="hidden" name="productId" value="${product.id}">
            Nome:              <g:textField name="name" value="${user?.username}"/> <br>
            Endereço:          <g:textField name="adress" value="${user?.adress}"/> <br>
            Quantidae:         <g:field name="amount" type="number" min="1" max="${product.stock}"/> <br>
            Forma de pagamento:
            <select name="paymentType">
                <option value="boleto">Boleto</option>
                <option value="creditCard">Cartão</option>
            </select>
            Forma de entrega:
            <select name="deliverOption">
                <option value="sedex">Sedex</option>
                <option value="correios">Correios</option>
            </select>
            <g:submitButton name="submitButton" value="Finalizar pedido" />
        </g:uploadForm>

        <a href="${g.createLink(action: 'index')}">
            <button type="button">Voltar</button>
        </a>

    </body>
</html>