<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="layout" content="main"/>
    <title>Gerenciamento de produtos</title>
</head>

<body>
<h1>Catálogo de produtos:</h1>

<table>
    <tr>
        <th>Imagem</th>
        <th>Nome</th>
        <th>Descrição</th>
        <th>Preço</th>
        <th>Preço Desconto</th>
        <th>Estoque</th>
    </tr>
    <g:each in="${productList}">
        <g:if test="${it.status && (it.stock > 0)}">
            <tr>
                <td>
                    <img src="data:image/png;base64,${it.image.encodeBase64()}"/>
                </td>
                <td>${it.name}</td>
                <td>${it.description}</td>
                <td>${it.fullPrice}</td>
                <td>${it.discountPrice}</td>
                <td>${it.stock}</td>
                <td>
                    <a href="${g.createLink(action: 'chart', id: it.id)}">
                        <button type="button">Comprar</button>
                    </a>
                </td>
            </tr>
        </g:if>
    </g:each>
</table>
</body>
</html>