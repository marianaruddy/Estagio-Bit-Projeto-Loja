package hitstore

import grails.plugin.springsecurity.annotation.Secured

class CatalogoController {

    def productService

    def index() {

        def productList = Product.list()

        render view: 'index', model: [productList: productList]
    }

    @Secured (['ROLE_USER'])
    def chart(Product product) {

        def user = this.getAuthenticatedUser()

        render view: 'chart' , model: [product: product, user: user]
    }

    @Secured (['ROLE_USER'])
    def checkout (Order order){

        def product = Product.findById(order.productId)
        product.stock -= order.amount
        order.status = "Aguardando pagamento"
        order.total = product.discountPrice * order.amount
        println(order.total)
        order.save()
        product.save()

//        sendMail {
//            from "vinilouzada.souza@gmail.com"
//            to "marianamrsantos@gmail.com"
//            subject "Nova compra"
//            text "Teste"
//        }

        redirect(action: 'index')
    }
}
