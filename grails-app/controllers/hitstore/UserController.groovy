package hitstore

import hitStore.auth.*

class UserController {

    def index() {}

    def save(User user) {
        def roleUser = Role.findByAuthority("ROLE_USER")
        user.authorities = [roleUser]
        user.save()

        redirect(action: 'index')
    }
}
