package hitstore

import grails.plugin.springsecurity.annotation.Secured

@Secured(['ROLE_ADMIN'])
class ProductController {

    def index() {

        def productList = Product.list()

        render view: 'index', model: [productList: productList]
    }

    def create() {

        render view: 'productForm'
    }

    def save(Product product) {
        product.save()

        redirect(action: 'index')
    }


    def edit(Product product) {

        render view: 'productForm', model: [product: product]
    }

    def delete(Product product) {

        product.delete()

        redirect(action: 'index')
    }
}

