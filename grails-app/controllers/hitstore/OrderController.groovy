package hitstore

import grails.plugin.springsecurity.annotation.Secured

@Secured(['ROLE_ADMIN'])
class OrderController {

    def index() {

        def orderList = Order.list()

        render view: 'index', model: [orderList: orderList]
    }
}
