package hitstore

import org.bson.types.ObjectId

class Order {

    ObjectId id
    String name
    String adress
    Integer amount
    String productId
    String status
    Double total
    String paymentType
    String deliverOption

    static constraints = {
    }
}
