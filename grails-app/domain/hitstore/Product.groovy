package hitstore

import org.bson.types.ObjectId

class Product {

    ObjectId id
    String name
    String description
    Boolean status
    Double fullPrice
    Double discountPrice
    byte[] image
    Integer stock
    Date createdDate = new Date()
    Date lastUpdated = new Date()
    String orderId

    static constraints = {
        name nullable: false
        description nullable: false
        status nullable: false
        fullPrice nullable: false
        discountPrice nullable: false
        image nullable: false
        stock nullable: false
        createdDate nullable: false
        lastUpdated nullable: false
        orderId nullable: true
    }
}
