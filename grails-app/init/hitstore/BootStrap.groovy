package hitstore
import hitStore.auth.*

class BootStrap {

    def init = { servletContext ->

        def roleAdmin = Role.findByAuthority("ROLE_ADMIN")?:new Role(authority: "ROLE_ADMIN").save(flush:true)

        def user = User.findByUsername("admin")?:new User(username:"admin",password:"admin",email: "admin@gmail.com").save(flush:true)

        if (!user.authorities) {
            user.authorities = [roleAdmin]
            user.save flush:true
        }
    }
    def destroy = {
    }
}
